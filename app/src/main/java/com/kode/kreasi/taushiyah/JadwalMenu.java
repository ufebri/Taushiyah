package com.kode.kreasi.taushiyah;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kode.kreasi.taushiyah.Adapter.JadwalAdapter;
import com.kode.kreasi.taushiyah.Content.DetailJadwalActivity;
import com.kode.kreasi.taushiyah.Utils.BaseApps;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JadwalMenu extends Fragment {


    JadwalAdapter jadwalAdapter;
    List<Jadwal> schduleCard = new ArrayList<>();

    private ProgressDialog progressDialog;

    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;

    public static JadwalMenu newInstance() {
        JadwalMenu fragment = new JadwalMenu();
        return fragment;
    }

    public JadwalMenu() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jadwal_menu, container, false);

        RecyclerView rec_schdule = view.findViewById(R.id.rec_Jadwal);
        rec_schdule.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        jadwalAdapter = new JadwalAdapter(schduleCard);

        rec_schdule.setAdapter(jadwalAdapter);

        getJadwal();

        initComponent(view);

        return view;

    }

    private void initComponent(View view) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.pleaseWait));
        progressDialog.setMessage(getString(R.string.dataLoadead));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        recyclerView = view.findViewById(R.id.rec_Jadwal);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);

    }

    private void getJadwal() {

        BaseApps.service.getJadwal().enqueue(new Callback<List<Jadwal>>() {
            @Override
            public void onResponse(Call<List<Jadwal>> call, Response<List<Jadwal>> response) {
                schduleCard.addAll(response.body());
                Log.d("Berhasil Terhubung", response.toString() + response.body().size());
                schduleCard.size();
                jadwalAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

                recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailJadwalActivity.class);
                            intent.putExtra("Judul", schduleCard.get(position).getJadwalName());
                            intent.putExtra("Deskripsi",schduleCard.get(position).getDescription());
                            intent.putExtra("Poster", schduleCard.get(position).getMasjidPhoto());
                            intent.putExtra("Tanggal", schduleCard.get(position).getJadwalDatetime());
                            intent.putExtra("NamaUlama", schduleCard.get(position).getUlamaName());
                            intent.putExtra("FotoUlama", schduleCard.get(position).getUlamaPhoto());
                            intent.putExtra("AlamatMasjid", schduleCard.get(position).getMasjidAddress());
                            intent.putExtra("NamaMasjid", schduleCard.get(position).getMasjidName());
                            intent.putExtra("TitleUlama", schduleCard.get(position).getUlamaTitle());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Jadwal>> call, Throwable t) {

                Log.d("Gagal Terhubung", t.getMessage());
            }
        });
    }

}
