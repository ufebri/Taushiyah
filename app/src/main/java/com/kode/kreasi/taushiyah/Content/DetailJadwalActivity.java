package com.kode.kreasi.taushiyah.Content;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;

import java.util.List;

public class DetailJadwalActivity extends AppCompatActivity {

    public ImageView poster, fotoulama;
    public TextView deskripsi, tanggal, ustadz, titleUstadz, namaMasjid, alamatMasjid;
    public CollapsingToolbarLayout judul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jadwal);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        judul = findViewById(R.id.collapsing_tollbar);
        judul.setTitle(getIntent().getStringExtra("Judul"));
        deskripsi = findViewById(R.id.tv_DeskripsiJadwal);
        deskripsi.setText(getIntent().getStringExtra("Deskripsi"));
        poster = findViewById(R.id.img_posterDetail);
        Glide.with(this)
                .load(getIntent().getStringExtra("Poster"))
                .placeholder(R.mipmap.ic_launcher_round)
                .into(poster);
        tanggal = findViewById(R.id.tv_TanggalJadwal);
        tanggal.setText(getIntent().getStringExtra("Tanggal"));
        ustadz = findViewById(R.id.tv_UlamaName);
        ustadz.setText(getIntent().getStringExtra("NamaUlama"));
        titleUstadz = findViewById(R.id.tv_UlamaTitle);
        titleUstadz.setText(getIntent().getStringExtra("TitleUlama"));
        fotoulama = findViewById(R.id.img_fotoUlama);
        Glide.with(this)
                .load(getIntent().getStringExtra("FotoUlama"))
                .placeholder(R.mipmap.ic_launcher_round)
                .into(fotoulama);

        namaMasjid = findViewById(R.id.tv_namaMasjidJadwal);
        namaMasjid.setText(getIntent().getStringExtra("NamaMasjid"));
        alamatMasjid = findViewById(R.id.tv_alamatMasjidJadwal);
        alamatMasjid.setText(getIntent().getStringExtra("AlamatMasjid"));


    }

}
