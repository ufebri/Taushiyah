package com.kode.kreasi.taushiyah;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        final TextInputLayout usernameWrapper = findViewById(R.id.usernameWrapper);
        usernameWrapper.setHint("Email");
        final TextInputLayout passwordWrapper = findViewById(R.id.passwordWrapper);
        passwordWrapper.setHint("Password");

    }
}
