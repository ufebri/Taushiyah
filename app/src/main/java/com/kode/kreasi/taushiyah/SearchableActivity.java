package com.kode.kreasi.taushiyah;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.kode.kreasi.taushiyah.Adapter.JadwalSearchAdapter;
import com.kode.kreasi.taushiyah.Adapter.MasjidSearchAdapter;
import com.kode.kreasi.taushiyah.Adapter.UlamaSearchAdapter;
import com.kode.kreasi.taushiyah.Utils.BaseApps;
import com.kode.kreasi.taushiyah.Utils.Interface.ApiService;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchableActivity extends AppCompatActivity {

    private RecyclerView recyclerView, recMasjid, recUlama;
    private List<Jadwal> mArrayList;
    private List<Masjid> masjidList;
    private List<Ulama> ulamaList;
    private RecyclerView.LayoutManager layoutManager;
    private JadwalSearchAdapter adapter;
    private UlamaSearchAdapter adapterUlama;
    private MasjidSearchAdapter adapterMasjid;
    private ApiService apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        initViews();
        getFilterSearch();
        getFilterMasjid();
        getFilterUlama();
    }

    private void initViews() {
        recyclerView = findViewById(R.id.rec_Searching);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recMasjid = findViewById(R.id.rec_searchingMasjid);
        layoutManager = new LinearLayoutManager(this);
        recMasjid.setLayoutManager(layoutManager);

        recUlama = findViewById(R.id.rec_searchingUlama);
        layoutManager = new LinearLayoutManager(this);
        recUlama.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);
    }

    private void getFilterSearch() {

        apiInterface = BaseApps.service;
        Call<List<Jadwal>> call = apiInterface.getJadwal();
        call.enqueue(new Callback<List<Jadwal>>() {
            @Override
            public void onResponse(Call<List<Jadwal>> call, Response<List<Jadwal>> response) {
                mArrayList = response.body();
                adapter = new JadwalSearchAdapter(mArrayList, SearchableActivity.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Jadwal>> call, Throwable t) {
                Toast.makeText(SearchableActivity.this, "Error On :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getFilterMasjid() {

        apiInterface = BaseApps.service;
        Call<List<Masjid>> call = apiInterface.getMasjid();
        call.enqueue(new Callback<List<Masjid>>() {
            @Override
            public void onResponse(Call<List<Masjid>> call, Response<List<Masjid>> response) {
                masjidList = response.body();
                adapterMasjid = new MasjidSearchAdapter(masjidList, SearchableActivity.this);
                recMasjid.setAdapter(adapterMasjid);
                adapterMasjid.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Masjid>> call, Throwable t) {
                Toast.makeText(SearchableActivity.this, "Error On :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getFilterUlama() {
        apiInterface = BaseApps.service;
        Call<List<Ulama>> call = apiInterface.getUlama();
        call.enqueue(new Callback<List<Ulama>>() {
            @Override
            public void onResponse(Call<List<Ulama>> call, Response<List<Ulama>> response) {
                ulamaList = response.body();
                adapterUlama = new UlamaSearchAdapter(ulamaList, SearchableActivity.this);
                recUlama.setAdapter(adapterUlama);
                adapterUlama.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Ulama>> call, Throwable t) {
                Toast.makeText(SearchableActivity.this, "Error On :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searching, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapter != null) adapter.getFilter().filter(newText);
                if (adapterMasjid != null) adapterMasjid.getFilter().filter(newText);
                if (adapterUlama !=null) adapterUlama.getFilter().filter(newText);
                return true;
            }
        });
    }

}
