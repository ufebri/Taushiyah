package com.kode.kreasi.taushiyah;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kode.kreasi.taushiyah.Helper.BottomNavigationBar;

public class MainActivity extends AppCompatActivity {


    public FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        floatingActionButton = findViewById(R.id.fab_AddItem);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

        initView();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(MainActivity.this, SearchableActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initView() {

        commitFragment(HomeMenu.newInstance());

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        BottomNavigationBar.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                android.support.v4.app.Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.action_home:
                        selectedFragment = HomeMenu.newInstance();
                        break;
                    case R.id.action_schdule:
                        selectedFragment = JadwalMenu.newInstance();
                        break;
                    case R.id.action_mosque:
                        selectedFragment = MasjidMenu.newInstance();
                        break;
                    case R.id.action_ulama:
                        selectedFragment = UlamaMenu.newInstance();
                        break;
                }
                commitFragment(selectedFragment);
                return true;
            }
        });

    }

    private void commitFragment(android.support.v4.app.Fragment selectedFragment) {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }

}
