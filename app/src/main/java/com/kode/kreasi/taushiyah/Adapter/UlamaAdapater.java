package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.List;

/**
 * Created by user on 03/07/18.
 */

public class UlamaAdapater extends RecyclerView.Adapter<UlamaAdapater.Holder> {

    private List<Ulama> ulamaList;
    Context context;

    public UlamaAdapater(List<Ulama> ulamaList) {
        this.ulamaList = ulamaList;
    }


    @NonNull
    @Override
    public UlamaAdapater.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ulama, parent,
                false);
        context = parent.getContext();
        return new Holder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull UlamaAdapater.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return ulamaList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {

            Ulama ulama = ulamaList.get(position);
            ImageView gambarUlama = itemView.findViewById(R.id.img_Ulama);
            TextView namaUlama = itemView.findViewById(R.id.tv_namaUlama);
            TextView alamatUlama = itemView.findViewById(R.id.tv_alamatUlama);


            Glide.with(context)
                    .load(ulama.getUlamaPhoto())
                    .placeholder(R.mipmap.ic_launcher)
                    .into(gambarUlama);
            namaUlama.setText(ulama.getUlamaName());
            alamatUlama.setText(ulama.getUlamaAddress());
        }
    }
}
