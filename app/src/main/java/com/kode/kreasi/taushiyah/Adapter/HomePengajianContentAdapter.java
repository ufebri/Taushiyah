package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;

import java.util.List;

/**
 * Created by Uray Febri on 05/07/18.
 */

public class HomePengajianContentAdapter extends RecyclerView.Adapter<HomePengajianContentAdapter.Holder> {
    private List<Jadwal> contentPengajianHome;
    Context context;

    public HomePengajianContentAdapter(List<Jadwal> contentPengajianHome) {
        this.contentPengajianHome = contentPengajianHome;
    }


    @NonNull
    @Override
    public HomePengajianContentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gambar,
                parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomePengajianContentAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return contentPengajianHome.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            Jadwal pengajian = contentPengajianHome.get(position);
            ImageView gambarContent = itemView.findViewById(R.id.img_gambarHome);
            TextView textContent = itemView.findViewById(R.id.tv_judulGambar);


            Glide.with(context)
                    .load(pengajian.getMasjidPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(gambarContent);
            textContent.setText(pengajian.getJadwalName());
        }
    }
}
