package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.SearchableActivity;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uray Febri on 10/07/18.
 */

public class MasjidSearchAdapter extends RecyclerView.Adapter<MasjidSearchAdapter.ViewHolder> implements Filterable {

    private List<Masjid> pencarianMasjid;
    private List<Masjid> pencarianFilter;
    Context context;

    public MasjidSearchAdapter(List<Masjid> pencarianMasjid, SearchableActivity searchableActivity) {
        this.pencarianMasjid = pencarianMasjid;
        this.pencarianFilter = pencarianMasjid;
    }

    @NonNull
    @Override
    public MasjidSearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MasjidSearchAdapter.ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return pencarianFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    pencarianFilter = pencarianMasjid;
                } else {
                    ArrayList<Masjid> filteredList = new ArrayList<>();

                    for (Masjid masjid : pencarianMasjid) {
                        if (masjid.getMasjidName().toLowerCase().contains(charString) ||
                                masjid.getMasjidAddress().toLowerCase().contains(charString)) {
                            filteredList.add(masjid);
                        }
                    }
                    pencarianFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = pencarianFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pencarianFilter = (ArrayList<Masjid>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            Masjid masjid = pencarianMasjid.get(position);

            TextView nama = itemView.findViewById(R.id.tv_namaSearch);
            TextView alamat = itemView.findViewById(R.id.tv_alamatSearch);
            ImageView gambar = itemView.findViewById(R.id.img_gambarSearch);

            Glide.with(context)
                    .load(masjid.getMasjidPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(gambar);
            nama.setText(masjid.getMasjidName());
            alamat.setText(masjid.getMasjidAddress());
        }
    }
}
