package com.kode.kreasi.taushiyah.Utils.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 03/07/18.
 */

public class Masjid {

    @SerializedName("masjidID")
    @Expose
    private String masjidID;
    @SerializedName("masjidName")
    @Expose
    private String masjidName;
    @SerializedName("regionalName")
    @Expose
    private String regionalName;
    @SerializedName("masjidAddress")
    @Expose
    private String masjidAddress;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("masjidPic1Name")
    @Expose
    private String masjidPic1Name;
    @SerializedName("masjidPic1Hp")
    @Expose
    private String masjidPic1Hp;
    @SerializedName("masjidPic1Email")
    @Expose
    private String masjidPic1Email;
    @SerializedName("masjidPic2Name")
    @Expose
    private String masjidPic2Name;
    @SerializedName("masjidPic2Hp")
    @Expose
    private String masjidPic2Hp;
    @SerializedName("masjidPic2Email")
    @Expose
    private String masjidPic2Email;
    @SerializedName("masjidPhoto")
    @Expose
    private String masjidPhoto;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("masjidStatus")
    @Expose
    private String masjidStatus;

    public String getMasjidID() {
        return masjidID;
    }

    public void setMasjidID(String masjidID) {
        this.masjidID = masjidID;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getRegionalName() {
        return regionalName;
    }

    public void setRegionalName(String regionalName) {
        this.regionalName = regionalName;
    }

    public String getMasjidAddress() {
        return masjidAddress;
    }

    public void setMasjidAddress(String masjidAddress) {
        this.masjidAddress = masjidAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMasjidPic1Name() {
        return masjidPic1Name;
    }

    public void setMasjidPic1Name(String masjidPic1Name) {
        this.masjidPic1Name = masjidPic1Name;
    }

    public String getMasjidPic1Hp() {
        return masjidPic1Hp;
    }

    public void setMasjidPic1Hp(String masjidPic1Hp) {
        this.masjidPic1Hp = masjidPic1Hp;
    }

    public String getMasjidPic1Email() {
        return masjidPic1Email;
    }

    public void setMasjidPic1Email(String masjidPic1Email) {
        this.masjidPic1Email = masjidPic1Email;
    }

    public String getMasjidPic2Name() {
        return masjidPic2Name;
    }

    public void setMasjidPic2Name(String masjidPic2Name) {
        this.masjidPic2Name = masjidPic2Name;
    }

    public String getMasjidPic2Hp() {
        return masjidPic2Hp;
    }

    public void setMasjidPic2Hp(String masjidPic2Hp) {
        this.masjidPic2Hp = masjidPic2Hp;
    }

    public String getMasjidPic2Email() {
        return masjidPic2Email;
    }

    public void setMasjidPic2Email(String masjidPic2Email) {
        this.masjidPic2Email = masjidPic2Email;
    }

    public String getMasjidPhoto() {
        return masjidPhoto;
    }

    public void setMasjidPhoto(String masjidPhoto) {
        this.masjidPhoto = masjidPhoto;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMasjidStatus() {
        return masjidStatus;
    }

    public void setMasjidStatus(String masjidStatus) {
        this.masjidStatus = masjidStatus;
    }

}
