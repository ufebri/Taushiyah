package com.kode.kreasi.taushiyah.Utils;

import android.app.Application;

import com.kode.kreasi.taushiyah.Utils.Interface.ApiService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 29/06/18.
 */

public class BaseApps extends Application {

    public static ApiService service;

    @Override
    public void onCreate() {
        super.onCreate();
        service = getRetrofit().create(ApiService.class);
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://163.53.192.162/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


}
