package com.kode.kreasi.taushiyah;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kode.kreasi.taushiyah.Adapter.HomeMasjidContentAdapter;
import com.kode.kreasi.taushiyah.Adapter.HomePengajianContentAdapter;
import com.kode.kreasi.taushiyah.Adapter.HomeUstadzContentAdapter;
import com.kode.kreasi.taushiyah.Content.DetailJadwalActivity;
import com.kode.kreasi.taushiyah.Content.DetailMasjidActivity;
import com.kode.kreasi.taushiyah.Content.DetailUlamaActivity;
import com.kode.kreasi.taushiyah.Utils.BaseApps;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeMenu extends Fragment implements View.OnClickListener {

    HomePengajianContentAdapter homePengajianContentAdapter;
    HomeMasjidContentAdapter homeMasjidContentAdapter;
    HomeUstadzContentAdapter homeUstadzContentAdapter;

    List<Jadwal> pengajianGrid = new ArrayList<>();
    List<Masjid> contentMasjidHome = new ArrayList<>();
    List<Ulama> contentUlama = new ArrayList<>();

    GridLayoutManager gridLayoutManager;
    RecyclerView recyclerView, recPengajian, recMasjid, recUlama;

    public static HomeMenu newInstance() {
        HomeMenu fragment = new HomeMenu();
        return fragment;
    }

    public HomeMenu() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home_menu, container, false);

        RecyclerView rec_contentPengajian = view.findViewById(R.id.rec_contentPengajian);
        rec_contentPengajian.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        homePengajianContentAdapter = new HomePengajianContentAdapter(pengajianGrid);
        rec_contentPengajian.setAdapter(homePengajianContentAdapter);

        RecyclerView rec_contentMasjidHome = view.findViewById(R.id.rec_contentMasjid);
        rec_contentMasjidHome.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        homeMasjidContentAdapter = new HomeMasjidContentAdapter(contentMasjidHome);
        rec_contentMasjidHome.setAdapter(homeMasjidContentAdapter);

        RecyclerView rec_contentUstadz = view.findViewById(R.id.rec_contentUstadz);
        rec_contentUstadz.setLayoutManager(new GridLayoutManager(rec_contentUstadz.getContext(), 4, GridLayoutManager.HORIZONTAL, false));

        homeUstadzContentAdapter = new HomeUstadzContentAdapter(contentUlama);
        rec_contentUstadz.setAdapter(homeUstadzContentAdapter);

        getContentPengajian();
        getContentMasjid();
        getContentUstadz();

        initComponent(view);

        TextView lihatPengajian = view.findViewById(R.id.tv_lihatPengajian);
        lihatPengajian.setOnClickListener(this);
        TextView lihatMasjid = view.findViewById(R.id.tv_lihatMasjid);
        lihatMasjid.setOnClickListener(this);
        TextView lihatUstadz = view.findViewById(R.id.tv_lihatUstandz);
        lihatUstadz.setOnClickListener(this);

        return view;
    }


    private void initComponent(View view) {
        recPengajian = view.findViewById(R.id.rec_contentPengajian);
        recPengajian.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recPengajian.setLayoutManager(gridLayoutManager);

        recMasjid = view.findViewById(R.id.rec_contentMasjid);
        recMasjid.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recMasjid.setLayoutManager(gridLayoutManager);

        recUlama = view.findViewById(R.id.rec_contentUstadz);
        recUlama.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        recUlama.setLayoutManager(gridLayoutManager);

    }


    private void getContentPengajian() {

        BaseApps.service.getJadwal().enqueue(new Callback<List<Jadwal>>() {
            @Override
            public void onResponse(Call<List<Jadwal>> call, Response<List<Jadwal>> response) {
                pengajianGrid.addAll(response.body());
                Log.d("Berhasil Terhubung", response.toString() + response.body().size());
                pengajianGrid.size();
                homePengajianContentAdapter.notifyDataSetChanged();

                recPengajian.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailJadwalActivity.class);
                            intent.putExtra("Judul", pengajianGrid.get(position).getJadwalName());
                            intent.putExtra("Deskripsi", pengajianGrid.get(position).getDescription());
                            intent.putExtra("Poster", pengajianGrid.get(position).getMasjidPhoto());
                            intent.putExtra("Tanggal", pengajianGrid.get(position).getJadwalDatetime());
                            intent.putExtra("NamaUlama", pengajianGrid.get(position).getUlamaName());
                            intent.putExtra("FotoUlama", pengajianGrid.get(position).getUlamaPhoto());
                            intent.putExtra("AlamatMasjid", pengajianGrid.get(position).getMasjidAddress());
                            intent.putExtra("NamaMasjid", pengajianGrid.get(position).getMasjidName());
                            intent.putExtra("TitleUlama", pengajianGrid.get(position).getUlamaTitle());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Jadwal>> call, Throwable t) {
                Log.d("Gagal Terhubung", t.getMessage());
            }
        });

    }

    private void getContentMasjid() {
        BaseApps.service.getMasjid().enqueue(new Callback<List<Masjid>>() {
            @Override
            public void onResponse(Call<List<Masjid>> call, Response<List<Masjid>> response) {
                contentMasjidHome.addAll(response.body());
                contentMasjidHome.size();
                homeMasjidContentAdapter.notifyDataSetChanged();

                recMasjid.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailMasjidActivity.class);
                            intent.putExtra("NamaMasjid", contentMasjidHome.get(position).getMasjidName());
                            intent.putExtra("AlamatMasjid", contentMasjidHome.get(position).getMasjidAddress());
                            intent.putExtra("RegionMasjid", contentMasjidHome.get(position).getRegionalName());
                            intent.putExtra("GambarMasjid", contentMasjidHome.get(position).getMasjidPhoto());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<Masjid>> call, Throwable t) {

            }
        });
    }

    private void getContentUstadz() {
        BaseApps.service.getUlama().enqueue(new Callback<List<Ulama>>() {
            @Override
            public void onResponse(Call<List<Ulama>> call, Response<List<Ulama>> response) {
                contentUlama.addAll(response.body());
                contentUlama.size();
                homeUstadzContentAdapter.notifyDataSetChanged();

                recUlama.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailUlamaActivity.class);
                            intent.putExtra("NamaUlama", contentUlama.get(position).getUlamaName());
                            intent.putExtra("FotoUlama", contentUlama.get(position).getUlamaPhoto());
                            intent.putExtra("TitleUlama", contentUlama.get(position).getUlamaTitle());
                            intent.putExtra("BioUlama", contentUlama.get(position).getUlamaBio());
                            intent.putExtra("RegionUlama", contentUlama.get(position).getRegionalName());
                            intent.putExtra("AlamatUlama", contentUlama.get(position).getUlamaAddress());
                            intent.putExtra("HpUlama", contentUlama.get(position).getUlamaHp1());
                            intent.putExtra("EmailUlama", contentUlama.get(position).getUlamaEmail());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Ulama>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = null;
        switch (view.getId()) {
            case R.id.tv_lihatPengajian:
                fragment = new JadwalMenu();
                replaceFragment(fragment);
                break;
            case R.id.tv_lihatMasjid:
                fragment = new MasjidMenu();
                replaceFragment(fragment);
                break;
            case R.id.tv_lihatUstandz:
                fragment = new UlamaMenu();
                replaceFragment(fragment);
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.Fra_contentPengajian, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
