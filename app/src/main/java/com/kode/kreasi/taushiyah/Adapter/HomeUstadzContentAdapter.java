package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.List;

/**
 * Created by user on 06/07/18.
 */

public class HomeUstadzContentAdapter extends RecyclerView.Adapter<HomeUstadzContentAdapter.Holder> {

    private List<Ulama> ustadzList;
    Context context;

    public HomeUstadzContentAdapter(List<Ulama> ustadzList) {
        this.ustadzList = ustadzList;
    }

    @NonNull
    @Override
    public HomeUstadzContentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gambar, parent, false);

        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeUstadzContentAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return ustadzList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            Ulama ustadz = ustadzList.get(position);
            ImageView gambarUstadz = itemView.findViewById(R.id.img_gambarHome);
            TextView alamatUstadz = itemView.findViewById(R.id.tv_judulGambar);

            Glide.with(context)
                    .load(ustadz.getUlamaPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(gambarUstadz);
            alamatUstadz.setText(ustadz.getUlamaAddress());
        }
    }
}
