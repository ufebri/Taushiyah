package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;

import java.util.List;

/**
 * Created by user on 03/07/18.
 */

public class MasjidAdapter extends RecyclerView.Adapter<MasjidAdapter.Holder> {

    private List<Masjid> listMosque;
    Context context;

    public MasjidAdapter(List<Masjid> listMosque) {
        this.listMosque = listMosque;
    }

    @NonNull
    @Override
    public MasjidAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_masjid,
                parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MasjidAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return listMosque.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            Masjid mosque = listMosque.get(position);
            ImageView imageMosque = itemView.findViewById(R.id.img_Masjid);
            TextView nameMosque = itemView.findViewById(R.id.tv_namaMasjid);
            TextView addressMosque = itemView.findViewById(R.id.tv_alamatMasjid);

            Glide.with(context)
                    .load(mosque.getMasjidPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round)
                    .into(imageMosque);
            nameMosque.setText(mosque.getMasjidName());
            addressMosque.setText(mosque.getMasjidAddress());
        }
    }
}
