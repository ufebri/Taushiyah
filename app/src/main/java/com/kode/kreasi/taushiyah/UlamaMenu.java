package com.kode.kreasi.taushiyah;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kode.kreasi.taushiyah.Adapter.UlamaAdapater;
import com.kode.kreasi.taushiyah.Content.DetailJadwalActivity;
import com.kode.kreasi.taushiyah.Content.DetailUlamaActivity;
import com.kode.kreasi.taushiyah.Utils.BaseApps;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UlamaMenu extends Fragment {


    UlamaAdapater ulamaAdapater;
    List<Ulama> ulamaList = new ArrayList<>();

    private ProgressDialog progressDialog;

    RecyclerView recyclerView;

    GridLayoutManager gridLayoutManager;

    public static UlamaMenu newInstance() {
        UlamaMenu fragment = new UlamaMenu();
        return fragment;
    }


    public UlamaMenu() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ulama_menu, container, false);


        RecyclerView rec_Ulama = view.findViewById(R.id.rec_ulama);
        rec_Ulama.setLayoutManager(new GridLayoutManager(getActivity(),1));

        ulamaAdapater = new UlamaAdapater(ulamaList);

        rec_Ulama.setAdapter(ulamaAdapater);

        getUlama();

        initComponent(view);

        return view;

    }

    private void initComponent(View view) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.pleaseWait));
        progressDialog.setMessage(getString(R.string.dataLoadead));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        recyclerView = view.findViewById(R.id.rec_ulama);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(),1);
        recyclerView.setLayoutManager(gridLayoutManager);
    }

    private void getUlama() {
        BaseApps.service.getUlama().enqueue(new Callback<List<Ulama>>() {
            @Override
            public void onResponse(Call<List<Ulama>> call, Response<List<Ulama>> response) {
                ulamaList.addAll(response.body());
                Log.d("Berhasil Terhubung", response.toString() + response.body().size());
                ulamaList.size();
                ulamaAdapater.notifyDataSetChanged();
                progressDialog.dismiss();

                recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailUlamaActivity.class);
                            intent.putExtra("NamaUlama", ulamaList.get(position).getUlamaName());
                            intent.putExtra("FotoUlama", ulamaList.get(position).getUlamaPhoto());
                            intent.putExtra("TitleUlama", ulamaList.get(position).getUlamaTitle());
                            intent.putExtra("BioUlama", ulamaList.get(position).getUlamaBio());
                            intent.putExtra("RegionUlama", ulamaList.get(position).getRegionalName());
                            intent.putExtra("AlamatUlama", ulamaList.get(position).getUlamaAddress());
                            intent.putExtra("HpUlama", ulamaList.get(position).getUlamaHp1());
                            intent.putExtra("EmailUlama", ulamaList.get(position).getUlamaEmail());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Ulama>> call, Throwable t) {
                Log.d("Gagal Terhubung", t.getMessage());

            }
        });

    }


}
