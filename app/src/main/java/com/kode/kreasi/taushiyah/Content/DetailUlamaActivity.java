package com.kode.kreasi.taushiyah.Content;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;

public class DetailUlamaActivity extends AppCompatActivity {

    public CollapsingToolbarLayout namaUlama;
    public ImageView fotoUlama;
    public TextView titleUlama,bioUlama,regionUlama, alamatUlama, hpUlama, emailUlama;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ulama);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_Ulama));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        namaUlama = findViewById(R.id.collapsing_Ulama);
        namaUlama.setTitle(getIntent().getStringExtra("NamaUlama"));
        fotoUlama = findViewById(R.id.img_gambarUlama);
        Glide.with(this)
                .load(getIntent().getStringExtra("FotoUlama"))
                .placeholder(R.mipmap.ic_launcher_round)
                .into(fotoUlama);
        bioUlama = findViewById(R.id.tv_BioUlama);
        bioUlama.setText(getIntent().getStringExtra("BioUlama"));
        titleUlama = findViewById(R.id.tv_TitleUlama);
        titleUlama.setText(getIntent().getStringExtra("TitleUlama"));
        alamatUlama = findViewById(R.id.tv_alamatUlamah);
        alamatUlama.setText(getIntent().getStringExtra("AlamatUlama"));
        regionUlama = findViewById(R.id.tv_regionUlama);
        regionUlama.setText(getIntent().getStringExtra("RegionUlama"));
        hpUlama = findViewById(R.id.tv_hpUlama);
        hpUlama.setText(getIntent().getStringExtra("HpUlama"));
        emailUlama = findViewById(R.id.tv_emailUlama);
        emailUlama.setText(getIntent().getStringExtra("EmailUlama"));
    }
}
