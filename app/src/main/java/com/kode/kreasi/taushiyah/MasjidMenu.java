package com.kode.kreasi.taushiyah;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kode.kreasi.taushiyah.Adapter.MasjidAdapter;
import com.kode.kreasi.taushiyah.Content.DetailJadwalActivity;
import com.kode.kreasi.taushiyah.Content.DetailMasjidActivity;
import com.kode.kreasi.taushiyah.Utils.BaseApps;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MasjidMenu extends Fragment {


    MasjidAdapter masjidAdapter;
    List<Masjid> masjidList = new ArrayList<>();
    RecyclerView recyclerView;


    GridLayoutManager gridLayoutManager;
    private ProgressDialog progressDialog;

    //Buat beralih tombol navigasi
    public static MasjidMenu newInstance() {
        MasjidMenu fragment = new MasjidMenu();
        return fragment;

    }

    public MasjidMenu() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_masjid_menu, container, false);

        RecyclerView rec_Masjid = view.findViewById(R.id.rec_Masjid);
        rec_Masjid.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        masjidAdapter = new MasjidAdapter(masjidList);
        rec_Masjid.setAdapter(masjidAdapter);

        getMasjid();

        initComponent(view);

        return view;


    }

    private void initComponent(View view) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.pleaseWait));
        progressDialog.setMessage(getString(R.string.dataLoadead));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        recyclerView = view.findViewById(R.id.rec_Masjid);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);

    }


    private void getMasjid() {
        BaseApps.service.getMasjid().enqueue(new Callback<List<Masjid>>() {
            @Override
            public void onResponse(Call<List<Masjid>> call, Response<List<Masjid>> response) {
                masjidList.addAll(response.body());
                Log.d("Berhasil Mengambil Data", response.toString() + response.body().size());
                masjidList.size();
                masjidAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

                recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }
                    });

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        if (child != null && gestureDetector.onTouchEvent(e)) {

                            Intent intent = new Intent(getActivity(), DetailMasjidActivity.class);
                            intent.putExtra("NamaMasjid", masjidList.get(position).getMasjidName());
                            intent.putExtra("AlamatMasjid",masjidList.get(position).getMasjidAddress());
                            intent.putExtra("RegionMasjid", masjidList.get(position).getRegionalName());
                            intent.putExtra("GambarMasjid", masjidList.get(position).getMasjidPhoto());
                            getActivity().startActivity(intent);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<Masjid>> call, Throwable t) {
                Log.d("Gagal Mengambil Data", t.getMessage());
            }
        });

    }

}
