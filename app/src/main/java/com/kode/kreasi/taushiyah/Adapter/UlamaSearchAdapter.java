package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.SearchableActivity;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uray Febri on 10/07/18.
 */

public class UlamaSearchAdapter extends RecyclerView.Adapter<UlamaSearchAdapter.ViewHolder> implements Filterable {

    private List<Ulama> pencarianUlama;
    private List<Ulama> pencarianFilter;
    Context context;

    public UlamaSearchAdapter(List<Ulama> pencarianUlama, SearchableActivity searchableActivity) {
        this.pencarianUlama = pencarianUlama;
        this.pencarianFilter = pencarianUlama;
    }

    @NonNull
    @Override
    public UlamaSearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UlamaSearchAdapter.ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return pencarianFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    pencarianFilter = pencarianUlama;
                } else {
                    ArrayList<Ulama> filteredList = new ArrayList<>();

                    for (Ulama ulama : pencarianUlama) {
                        if (ulama.getUlamaName().toLowerCase().contains(charString) ||
                                ulama.getUlamaAddress().toLowerCase().contains(charString)) {
                            filteredList.add(ulama);
                        }
                    }
                    pencarianFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = pencarianFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pencarianFilter = (ArrayList<Ulama>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nama, alamat;
        private ImageView gambar;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            nama = itemView.findViewById(R.id.tv_namaSearch);
            alamat = itemView.findViewById(R.id.tv_alamatSearch);
            gambar = itemView.findViewById(R.id.img_gambarSearch);

            Ulama ulama = pencarianUlama.get(position);

            Glide.with(context)
                    .load(ulama.getUlamaPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(gambar);
            nama.setText(pencarianFilter.get(position).getUlamaName());
            alamat.setText(pencarianFilter.get(position).getUlamaAddress());
        }
    }
}
