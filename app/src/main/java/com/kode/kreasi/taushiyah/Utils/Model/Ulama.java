package com.kode.kreasi.taushiyah.Utils.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 03/07/18.
 */

public class Ulama {
    @SerializedName("ulamaID")
    @Expose
    private String ulamaID;
    @SerializedName("ulamaName")
    @Expose
    private String ulamaName;
    @SerializedName("ulamaTitle")
    @Expose
    private String ulamaTitle;
    @SerializedName("ulamaPhoto")
    @Expose
    private String ulamaPhoto;
    @SerializedName("fahamName")
    @Expose
    private String fahamName;
    @SerializedName("ulamaBio")
    @Expose
    private String ulamaBio;
    @SerializedName("regionalName")
    @Expose
    private String regionalName;
    @SerializedName("ulamaAddress")
    @Expose
    private String ulamaAddress;
    @SerializedName("ulamaHp1")
    @Expose
    private String ulamaHp1;
    @SerializedName("ulamaHp2")
    @Expose
    private String ulamaHp2;
    @SerializedName("ulamaPhone")
    @Expose
    private String ulamaPhone;
    @SerializedName("ulamaEmail")
    @Expose
    private String ulamaEmail;
    @SerializedName("ulamaNo_rek")
    @Expose
    private String ulamaNoRek;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("ulamaStatus")
    @Expose
    private String ulamaStatus;

    public String getUlamaID() {
        return ulamaID;
    }

    public void setUlamaID(String ulamaID) {
        this.ulamaID = ulamaID;
    }

    public String getUlamaName() {
        return ulamaName;
    }

    public void setUlamaName(String ulamaName) {
        this.ulamaName = ulamaName;
    }

    public String getUlamaTitle() {
        return ulamaTitle;
    }

    public void setUlamaTitle(String ulamaTitle) {
        this.ulamaTitle = ulamaTitle;
    }

    public String getUlamaPhoto() {
        return ulamaPhoto;
    }

    public void setUlamaPhoto(String ulamaPhoto) {
        this.ulamaPhoto = ulamaPhoto;
    }

    public String getFahamName() {
        return fahamName;
    }

    public void setFahamName(String fahamName) {
        this.fahamName = fahamName;
    }

    public String getUlamaBio() {
        return ulamaBio;
    }

    public void setUlamaBio(String ulamaBio) {
        this.ulamaBio = ulamaBio;
    }

    public String getRegionalName() {
        return regionalName;
    }

    public void setRegionalName(String regionalName) {
        this.regionalName = regionalName;
    }

    public String getUlamaAddress() {
        return ulamaAddress;
    }

    public void setUlamaAddress(String ulamaAddress) {
        this.ulamaAddress = ulamaAddress;
    }

    public String getUlamaHp1() {
        return ulamaHp1;
    }

    public void setUlamaHp1(String ulamaHp1) {
        this.ulamaHp1 = ulamaHp1;
    }

    public String getUlamaHp2() {
        return ulamaHp2;
    }

    public void setUlamaHp2(String ulamaHp2) {
        this.ulamaHp2 = ulamaHp2;
    }

    public String getUlamaPhone() {
        return ulamaPhone;
    }

    public void setUlamaPhone(String ulamaPhone) {
        this.ulamaPhone = ulamaPhone;
    }

    public String getUlamaEmail() {
        return ulamaEmail;
    }

    public void setUlamaEmail(String ulamaEmail) {
        this.ulamaEmail = ulamaEmail;
    }

    public String getUlamaNoRek() {
        return ulamaNoRek;
    }

    public void setUlamaNoRek(String ulamaNoRek) {
        this.ulamaNoRek = ulamaNoRek;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUlamaStatus() {
        return ulamaStatus;
    }

    public void setUlamaStatus(String ulamaStatus) {
        this.ulamaStatus = ulamaStatus;
    }
}
