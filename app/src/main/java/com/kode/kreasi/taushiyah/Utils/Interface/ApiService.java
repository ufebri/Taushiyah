package com.kode.kreasi.taushiyah.Utils.Interface;

import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;
import com.kode.kreasi.taushiyah.Utils.Model.Ulama;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Uray Febri on 29/06/18.
 */

public interface ApiService {
    @GET("tausyiah/api/jadwal/all?X-Api-Key=4B31F0BD729F71880BDA51242A56CDAE")
    Call<List<Jadwal>> getJadwal();

    @GET("tausyiah/api/masjid/all?X-Api-Key=4B31F0BD729F71880BDA51242A56CDAE")
    Call<List<Masjid>> getMasjid();

    @GET("tausyiah/api/ulama/all?X-Api-Key=4B31F0BD729F71880BDA51242A56CDAE")
    Call<List<Ulama>> getUlama();
}
