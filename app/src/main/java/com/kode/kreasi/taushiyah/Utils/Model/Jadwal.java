package com.kode.kreasi.taushiyah.Utils.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 29/06/18.
 */

public class Jadwal {

    @SerializedName("jadwalID")
    @Expose
    private String jadwalID;
    @SerializedName("jadwalName")
    @Expose
    private String jadwalName;
    @SerializedName("jadwalDatetime")
    @Expose
    private String jadwalDatetime;
    @SerializedName("masjidName")
    @Expose
    private String masjidName;
    @SerializedName("masjidAddress")
    @Expose
    private String masjidAddress;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("masjidPhoto")
    @Expose
    private String masjidPhoto;
    @SerializedName("ulamaName")
    @Expose
    private String ulamaName;
    @SerializedName("ulamaTitle")
    @Expose
    private String ulamaTitle;
    @SerializedName("ulamaPhoto")
    @Expose
    private String ulamaPhoto;
    @SerializedName("fahamName")
    @Expose
    private String fahamName;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("link_video")
    @Expose
    private Object linkVideo;
    @SerializedName("tags")
    @Expose
    private Object tags;

    public String getJadwalID() {
        return jadwalID;
    }

    public void setJadwalID(String jadwalID) {
        this.jadwalID = jadwalID;
    }

    public String getJadwalName() {
        return jadwalName;
    }

    public void setJadwalName(String jadwalName) {
        this.jadwalName = jadwalName;
    }

    public String getJadwalDatetime() {
        return jadwalDatetime;
    }

    public void setJadwalDatetime(String jadwalDatetime) {
        this.jadwalDatetime = jadwalDatetime;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getMasjidAddress() {
        return masjidAddress;
    }

    public void setMasjidAddress(String masjidAddress) {
        this.masjidAddress = masjidAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMasjidPhoto() {
        return masjidPhoto;
    }

    public void setMasjidPhoto(String masjidPhoto) {
        this.masjidPhoto = masjidPhoto;
    }

    public String getUlamaName() {
        return ulamaName;
    }

    public void setUlamaName(String ulamaName) {
        this.ulamaName = ulamaName;
    }

    public String getUlamaTitle() {
        return ulamaTitle;
    }

    public void setUlamaTitle(String ulamaTitle) {
        this.ulamaTitle = ulamaTitle;
    }

    public String getUlamaPhoto() {
        return ulamaPhoto;
    }

    public void setUlamaPhoto(String ulamaPhoto) {
        this.ulamaPhoto = ulamaPhoto;
    }

    public String getFahamName() {
        return fahamName;
    }

    public void setFahamName(String fahamName) {
        this.fahamName = fahamName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Object getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(Object linkVideo) {
        this.linkVideo = linkVideo;
    }

    public Object getTags() {
        return tags;
    }

    public void setTags(Object tags) {
        this.tags = tags;
    }
}
