package com.kode.kreasi.taushiyah.Content;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;

public class DetailMasjidActivity extends AppCompatActivity {

    public CollapsingToolbarLayout collapsingToolbarLayout;
    public ImageView imageView;
    public TextView alamatMasjid, regionMasjid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_masjid);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_Masjid));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = findViewById(R.id.collapsing_NamaMasjid);
        collapsingToolbarLayout.setTitle(getIntent().getStringExtra("NamaMasjid"));

        imageView = findViewById(R.id.img_gambarMasjid);
        Glide.with(this)
                .load(getIntent().getStringExtra("GambarMasjid"))
                .placeholder(R.mipmap.ic_launcher_round)
                .into(imageView);

        alamatMasjid = findViewById(R.id.tv_AlamatMasjidDetil);
        alamatMasjid.setText(getIntent().getStringExtra("AlamatMasjid"));
        regionMasjid = findViewById(R.id.tv_RegionMasjid);
        regionMasjid.setText(getIntent().getStringExtra("RegionMasjid"));


    }
}
