package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Masjid;

import java.util.List;

/**
 * Created by Uray Febri on 06/07/18.
 */

public class HomeMasjidContentAdapter extends RecyclerView.Adapter<HomeMasjidContentAdapter.Holder> {
    private List<Masjid> masjidList;
    Context context;
    int LimitShow = 1;

    public HomeMasjidContentAdapter(List<Masjid> masjidList) {
        this.masjidList = masjidList;
    }

    @NonNull
    @Override
    public HomeMasjidContentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gambar, parent, false);

        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeMasjidContentAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        if (LimitShow * 4 > masjidList.size()) {
            return masjidList.size();
        } else {
            return LimitShow * 4;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);

        }

        public void bind(int position) {
            Masjid mesjid = masjidList.get(position);
            ImageView coverMasjid = itemView.findViewById(R.id.img_gambarHome);
            TextView namaMasjid = itemView.findViewById(R.id.tv_judulGambar);

            Glide.with(context)
                    .load(mesjid.getMasjidPhoto())
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(coverMasjid);
            namaMasjid.setText(mesjid.getMasjidName());

        }
    }
}
