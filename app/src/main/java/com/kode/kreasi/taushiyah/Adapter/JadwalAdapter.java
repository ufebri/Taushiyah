package com.kode.kreasi.taushiyah.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kode.kreasi.taushiyah.R;
import com.kode.kreasi.taushiyah.Utils.Model.Jadwal;

import java.util.List;

/**
 * Created by Uray Febri on 29/06/18.
 */

public class JadwalAdapter extends RecyclerView.Adapter<JadwalAdapter.Holder> {

    private List<Jadwal> listJadwal;
    private Context context;

    public JadwalAdapter(List<Jadwal> listJadwal) {
        this.listJadwal = listJadwal;
    }


    @NonNull
    @Override
    public JadwalAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jadwal,
                parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JadwalAdapter.Holder holder, final int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return listJadwal.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        public Holder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {
            Jadwal jadwal = listJadwal.get(position);
            ImageView gambarMesjid = itemView.findViewById(R.id.img_Cover);
            TextView judulAcara = itemView.findViewById(R.id.tv_Judul);
            TextView namaMasjid = itemView.findViewById(R.id.tv_masjid);
            TextView lokasiMasjid = itemView.findViewById(R.id.tv_Lokasi);
            TextView tanggalAcara = itemView.findViewById(R.id.tv_Tanggal);

            Glide.with(context)
                    .load(jadwal.getMasjidPhoto())
                    .placeholder(R.mipmap.ic_launcher)
                    .into(gambarMesjid);
            judulAcara.setText(jadwal.getJadwalName());
            namaMasjid.setText(jadwal.getMasjidName());
            lokasiMasjid.setText(jadwal.getMasjidAddress());
            tanggalAcara.setText(jadwal.getJadwalDatetime());

        }
    }
}
